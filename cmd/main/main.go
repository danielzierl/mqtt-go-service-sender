package main

import "gitlab.com/danielzierl/mqtt-go-service-sender/internal"

func main() {
	internal.SetupLoggingAuto()
	defer internal.CloseLogFile()

	internal.RunMockSender()
}
