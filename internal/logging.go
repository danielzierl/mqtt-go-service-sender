package internal

import (
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const LOG_FILE_NAME = "main.log"
const LOG_DIR = "logs"

var logFile *os.File

func SetupLoggingAuto() {
	// Creates new log file in the "git project root"/logs/main.log
	// Needs to be closed in main
	rootDir, _ := GetGitRootDir()
	SetupLogging(filepath.Join(rootDir, LOG_DIR))
}

func SetupLogging(outputDir string) {
	logPath := filepath.Join(outputDir, LOG_FILE_NAME)

	logFile, err := os.OpenFile(logPath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		panic(err)
	}
	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)
	log.Println("Logging setup complete")
}

func CloseLogFile() {
	log.Println("Closing log file")
	if logFile != nil {
		logFile.Close()
	}
}

func GetGitRootDir() (string, error) {
	// Run the "git rev-parse --show-toplevel" command to get the root directory of the Git repository
	cmd := exec.Command("git", "rev-parse", "--show-toplevel")
	output, err := cmd.Output()
	if err != nil {
		return "", err
	}

	// Convert the command output to a string and remove any trailing newline characters
	rootDir := strings.TrimSpace(string(output))

	return rootDir, nil
}
