package internal

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/joho/godotenv"
)

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	log.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	log.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	log.Printf("Connect lost: %v", err)
}

func RunMockSender() {

	err := godotenv.Load()
	if err != nil {
		log.Fatal(".env not found")
	}
	var broker = os.Getenv("MOSQUITTO_HOST")
	var port = 1883
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", broker, port))
	opts.SetClientID("go_mqtt_sender")
	opts.SetUsername(os.Getenv("USER"))
	opts.SetPassword(os.Getenv("PASSWORD"))
	opts.SetDefaultPublishHandler(messagePubHandler)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	publish(client)
	client.Disconnect(250)
}
func publish(client mqtt.Client) {
  for i:=0;;i++ {
    fmt.Println(i)
        temp:= fmt.Sprintf("%f",rand.Float64()+20)
        text := "{\"team_name\": \"white\", \"timestamp\": \"2020-03-24T15:26:05.336974\", \"temperature\": "+temp+", \"humidity\": 64.5}"
        token := client.Publish("ite/red/test", 0, false, text)
        token.Wait()
        time.Sleep(time.Second*1)
    }
}
