 FROM golang:1.21.1-alpine
 WORKDIR /app
 COPY . .
 RUN go build -o sender ./cmd/main/main.go
 CMD ["./sender"]
